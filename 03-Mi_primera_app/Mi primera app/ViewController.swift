//
//  ViewController.swift
//  Mi primera app
//
//  Created by Canek on 1/22/20.
//  Copyright © 2020 NebuLab. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var titleImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var pressButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.titleImageView.isHidden = true
        self.backgroundImageView.isHidden = true
        self.pressButton.isHidden = false
    }
    
    @IBAction func buttonPressed(_ sender: UIButton) {
        print("Botón presionado")
        
        self.titleImageView.isHidden = false
        self.backgroundImageView.isHidden = false
        self.pressButton.isHidden = true
    }
    
}
