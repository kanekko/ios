import UIKit

///// Números enteros ////
// Swift proporciona enteros con signo y sin signo de
// 8, 16, 32 y 64 bits representados por
// UInt8: entero sin signo de 8 bits
// Int32: entero de 32 bits

let minValue = UInt8.min // El valor mínimo de un UInt8 es 0
let maxValue = UInt8.max // El valor máximo de un UInt8 es (2^8)-1=255

// Int, en plataformas de 32 bits Int tendrá el tamaño de Int32
// en plataformas de 64 bits Int tendrá el tamaño de Int64
// mode empleo:
let miEntero: Int = 7
let miEnteroPositivo: UInt = 32

// Valores de 32 bits
print("Valorres de 32 bits: (\(Int32.min), \(Int32.max))")
// Valores de 64 bits
print("Valorres de 64 bits: (\(Int64.min), \(Int64.max))")


//// Números decimales ////
// float y double
let pi = 3.14169265 // Pi en este caso es un número Double
let minT = -273.15 //minT también es un Double

let meaningOfLife = 42 // El sentido de la vida, es un número entero y vale 42

let anotherPi = 3 + 0.14159265 // anotherPi también es un Double


//// Literales numéricos ////

// Número en formato Decimal
let decimalNumber = 17
// Número en formato Binario (Rango 0-1)
let decimalBinario = 0b10001 // (16*1) + (8*0) + (4*0) + (2*0) + (1*1) = 17 en binario
// Número en formato Octal (Rango 0-7)
let decimalOctal = 0o21 // (8*2) + (1*1) = 17 en octal
// Número en formato Hexadecimal (Rango 0-9ABCDEF)
let decimalHex = 0x11 // (16*1) + (1*1) = 17 en hexadecimal

// Números con mantiza y exponente
let number = 1.25e2 // (1.25) * 10^2 [Notación científica]
let number2 = 1.25e-2 // 0.0125
let number3 = 0xFp2 // 15 * (2^2) = 60
let number4 = 0xFp-2 // 15 * (2^-2) = 3.75

// Ej: expresar el numero: 12.1875
let n1 = 12.1875
let n2 = 1.21875e1
let n3 = 0xC.3p0

// Los literales númericos se pueden formatear para que sean más fáciles de leer
// agregando 0's y underscores:
let paddedDouble  = 123.456
let paddedDouble2 = 00000123.456

let oneMillion  = 1000000
let oneMillion2 = 1_000_000

let overOneMillion = 1_000_000.000_000_1


//// Castings ////
// Los castings permiten transformar tipos de datos

// Int8: -128 a 127
//UInt8: 0 a 255
// Errores tipicos:
//let myUnit:UInt8 = -1 // NO
//let tooBigUint:Int8 = Int8.max + 1 // NO

// sumando un UInt8 y Uint16
let twoThousand: UInt16 = 2_000
let one: UInt8 = 1
let twoThousandAndOne = twoThousand + UInt16(one) // twoThousandAndOne es un UInt16 por los sumandos

let three = Double(3)
let piFloat = Float(3.14159265) // Se pierde precisión y se rendondea a: 3.141593
let piImt = Int(3.14159265)
let piDouble = three + 0.14159265

// truncamiento: casting de Double a Int
let newN  = Int(5.85)
let newN2 = Int(-5.85)

// Type alias //
typealias AudioSample = UInt16
var maxAmplitude = AudioSample.max
var mySample : [AudioSample] = [0,6,32,76,87]
