import UIKit

//// Errores ////
// Una función con la palabra throws indica que puede lanzar un error:
func canThrowError() throws{
    
}

// do crea una zona de trabajo, que permite propagar uno o más errores
do{
    try canThrowError()
    // Si no ha lanzado error, el código sigue por aquí.
} catch {
    // Si hay un error, entramos en la zona del catch
}


// Ej:
func makeASandwich() throws{ }

func eatASandwich(){ }

func washTheDishes(){ }

func buyIngredients(){ }

do{
    try makeASandwich()
    eatASandwich()
} catch {
    // No tenemos platos limpios
    washTheDishes()
} catch {
    // No tengo ingredientes para el sandwich
    buyIngredients()
}


//// Aserciones y precondiciones ////
// comprobar lo que pasa en tiempo de ejecución
// verificar una serie de condiciones antes de que el código siga adelante
// aseciones no ayudan a encontrar fallos durante el desarrollo
// mientras que las precondiciones nos ayudar a detectar esos fallos en producción
// las aserciones solo entran en fase de desarrollo o pruebas
// y las precondiciones solo son lanzadas en producción

// Ejemplo de aserción:
let age = -8

// sintaxis:
//assert(age>=0, "Una persona no puede tener edad negativa")
//assert(age>=0)

// modo de empleo:
/*if age > 10 {
    print("Puedes subir a la montaña rusa")
} else if age > 0 {
    print("Eres muy pequeño para la montaña rusa")
} else {
    assertionFailure("Una persona no puede tener edad negativa")
}*/

// Ejemplo de precondiciones: (detectar una posible condiciones falsa)
let index = -5
precondition(index>=0,"Los índices deben ser mayores o iguales a cero")
//preconditionFailure("Aquí va el mensaje") // análogo a assertionFailure
