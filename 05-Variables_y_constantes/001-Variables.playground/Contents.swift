import UIKit

// Varialbes: valores que van a cambiar
var currentLoginAttempt = 0
// Constantes: valores de código que nunca cambiarán
let maximumNumberOfLogins = 10
// ej:
var x=0.0, y=0.0, z=0.0


/// Type annotation ///
var welcomeMessage: String = ""
welcomeMessage = "Hola"
print(welcomeMessage)

welcomeMessage = "Bonjour"
print(welcomeMessage)

//print([1,2,3], separator: "-", terminator: ".")
print("Me acaban de saludar diciendo: \(welcomeMessage)")

var red, green, blue: Double


/// Nomenclatura ///
let π = 3.14159265

let 🐶🐮 = "perrovaca"
🐶🐮

//let 3a = 45 //NO
//let a3 = 45 //SÍ

//let language = "Swift" // No es permitido cambiar el valor de una constante
//language = "Swift4"

let cat="🐈"; print(cat)

/*
 Esto es un comentario
 que ocupa varias línea
 de código en Swift.
 */

/*
 Esto es la primera línea del comentario.
 /*
  Esto es el segundo comentario, anidado al primero.
 */
 Esto es el comentario que finaliza el multilínea.
 */
