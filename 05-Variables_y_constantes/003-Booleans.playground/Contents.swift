import UIKit

//// Booleanos ////
var myBool: Bool //myBool puede valer true o false
let orangesAreOrange = true
let turnipAreDelicious = false

if turnipAreDelicious{
    print("Mmmmmm, ¡Que ricos son los nabos con la paella!")
} else {
    print("Quita ese nabo asqueroso de mi vista")
}

// NO:
//let i=3
//let myComparison = (i==3)
//if i==3{
//    print("tengo un 3")
//}


//// Tuplas ////
let httpError: (Int, String) = (404, "Error, not found")
let coordinates: (Int, Int, Int)
let password: (String, Bool)

// separando tupla en dos partes:
let (stautsCode, statusMessage) = httpError
print("El código de error es \(stautsCode) y el mensaje que devuelve es \(statusMessage)")

let (justStatusCode, _) = httpError
print("Me acabn de dcir que el código es \(justStatusCode)")

// accediendo a los elementos de una tupla:
print("La primera parte es: \(httpError.0) y la segunda parte es: \(httpError.1)")

// encapsulando la información en una tupla:
let http200Satus = (statusCode:200, description: "OK")
print(http200Satus.statusCode)
print(http200Satus.description)


//// Optionals ////
let possibleNumber = "123"
let convertedNumber = Int(possibleNumber) //No es un Int, es un Int?
print(convertedNumber)

// nil no se puede asignar a constantes y varialbes no opcionales:
var serverResponseCode: Int? = 404
serverResponseCode = nil

// nil es desconocer el valor en swift (valor que es la ausencia de valor)
// el valor de una varialbe opcional por defecto es nil:
var answer: String? // answer=nil (!= null)

// "if y else" para extraer valores de variables opcionales:
if serverResponseCode != nil {
    print("El código del servidor no es nulo, no que vale \(serverResponseCode)")
    print("El código del servidor no es nulo, no que vale \(serverResponseCode!)") //ungrapping: recuperar el valor de la variable
}
//print(serverResponseCode!) //NO HACER

// técnica de optional binding para asegurarnos que un optional contiene el valor
// y en caso de que lo contenga utilizarlo
//possibleNumber = "hola" // prueba para el else
if let actualNumber = Int(possibleNumber){ //actualNumber es un Int
    print("Mi número actual es \(actualNumber)")
} else {
    print("\(possibleNumber) no contien un número entero")
}

// ej:
if let firstNumber = Int("4"),
   let secondNumber = Int("53"),
    firstNumber<secondNumber && secondNumber<100{
    print("\(firstNumber) < \(secondNumber) < 100")
}

if let firstNumber = Int("4"){
    if let secondNumber = Int("53"){
        if firstNumber<secondNumber && secondNumber<100{
            print("\(firstNumber) < \(secondNumber) < 100")
        }
   }
}

// accediendo a valores optional de manera implicita,
// revisar el valor y trabajar directamente con el:
let possibleString: String? = "Esto es un string opcional"
let forcedString: String = possibleString! // Solo hacer si la variale no es nula.

let assumedString: String! = "String ya con valor"
let implicitString: String = assumedString // NO necesito exclamación, porque es seguro que existe el valor.

if assumedString != nil {
    print(assumedString)
}
if let definitiveString = assumedString{
    print(assumedString)
}
