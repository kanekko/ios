//// Swift 3.0 ////

// var str = "Hello, playground"
// str.strimingByTrimmingCharactersInSet()
// str.trimmingCharacters(in:)

// dismissViewControllerAnimated(true)
// dismiss(animated:true)

// UIColor.whiteColor()
// UIColor.white

// NSTextAlignment.Right
// NSTextAlignment.right
// .right



//// Swift 4.0 ////

// Multiline Strings //
// let myStr = "Hola que tal\nMe llamo Canek"
// print(myStr)
let myStr = """
                En un lugar de la Mancha
                de cuyo nombre no quiero acordarme

             Y también se permiten "comillas dobles" en el texto.
             """
//print(myStr)

// Strings y colecciones //
let testStr = "Hola, soy Canek"

// OLD: let characters = testStr.characters
for c in testStr {
//    print(c)
}

// print(testStr.reversed())
//print("String invertido")
for c in testStr.reversed(){
//    print(c)
}

// Rangos unilaterales //
let food = ["Pizza", "Panacotta", "Spaguetti", "Lubina a la sal", "Mejillón tigre", "Ensalada César"]
let italianFood = food[..<3]
let fishFood = food[3...]

let menu = food[1...3] // rangos bilaterales

// Diccionarios //
let cities = ["Madrid":3_165_541, "Barcelona":1_608_746, "Sevilla":690_566,
              "Palma de Mallorca":402_949, "Oviedo":220_567, "Santa Cruz de Tenerife":203_585]

let massiveCities = cities.filter{$0.value>500_000}
//massiveCites[0].values // Swift 3
print(massiveCities)

let population = cities.map{$0.value*3} // Mapping
print(population)

let newPopulation = cities.mapValues{"\($0/1_000) miles de personas"}
print(newPopulation)

// let groupedCities = Dictionary(grouping:cities.keys){$0.characters.first!}
// print(groupedCities)

let groupedPopulationCities = Dictionary(grouping:cities.keys){$0.count}
print(groupedPopulationCities)

let person = ["name":"Canek",
              "city":"México"]
// let name = person["name",default:"Unknown"] //OK
// let name = person["nombre",default:"Unknown"] //NO-OK
let name = person["nombre"] ?? "Unknown"
print(name)



let favouriteSinger = ["Coldplay", "Madonna", "Justin Beiber", "Madonna",
                       "Paco de Lucía", "Madonna", "Coldplay"]
var favouriteCounts = [String:Int]()

for singer in favouriteSinger {
    favouriteCounts[singer,default:0] += 1
}
print(favouriteCounts)


let people = [("John","Lennon"),("Paul","McCartney"),("George","Harrison"),
              ("Ringo","Starr")]
let namesDict = Dictionary(uniqueKeysWithValues:people)
// print(namesDict)
print(namesDict["Yokono"] ?? "Persona no invitada")
print(namesDict["Paul"] ?? "Persona no invitada")


let primeNumbers = [2,3,5,7,11,13]
let combinerNumbers = Dictionary(uniqueKeysWithValues: zip(1..., primeNumbers))
print(combinerNumbers)

let squaredNumbers = [1,4,9,16,25,36]
let combinerNumbers2 = Dictionary(uniqueKeysWithValues: zip(1..., squaredNumbers))
print(combinerNumbers2)
